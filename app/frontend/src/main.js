// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Navigation from './components/Navigation'
import Footer from './components/Footer'
import SpecialCaseForm from './components/SpecialCaseForm'
import StudentDetail from './components/StudentDetail'
import SelectClasses from './components/SelectClasses'

Vue.config.productionTip = false
Vue.component('navigation', Navigation)
Vue.component('footer-navigation', Footer)
Vue.component('special-case-form', SpecialCaseForm)
Vue.component('student-detail', StudentDetail)
Vue.component('select-classes', SelectClasses)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
