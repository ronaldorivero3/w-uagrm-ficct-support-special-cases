import Vue from 'vue'
import Router from 'vue-router'
import SpecialCaseForm from '@/components/SpecialCaseForm'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'SpecialCaseForm',
      component: SpecialCaseForm
    }
  ]
})
