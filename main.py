from flask import render_template, request, make_response
from app import create_app
from app.wkhtmltopdf_config import config as wkhtmltopdf_config
import pdfkit
from flask_cors import CORS, cross_origin
import json
from datetime import datetime

app = create_app()

cors = CORS(app)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/generate-documentation', methods=['POST'])
@cross_origin()
def generate():
    parsed_request = request.get_json()

    print(str(parsed_request))

    date = datetime.today().strftime('%d/%m/%Y')
    city_with_date = "Santa Cruz de la sierra {}".format(date)

    career = _get_career_data_from_json(parsed_request["career_data"])
    classes = _get_special_case_classes_from_json(parsed_request["career_data"]["classes"])

    context = {
        'city_with_date': city_with_date,
        'career': career,
        'special_case_classes': classes,
        'student': parsed_request["student"]
    }

    academic_progress_context = {
        'student': parsed_request["student"],
        'classes': parsed_request["career_data"]["classes"]
    }

    options = {
        'page-size': 'Letter',
    }

    # letter_html = render_template('special_case_letter.html',**context)
    # registration_form_html = render_template('special_case_registration_form.html',**context)
    academic_progress_html = render_template('academic_progress.html', **academic_progress_context)

    # pdf_letter= pdfkit.from_string(letter_html,False,configuration=wkhtmltopdf_config,options=options)
    # pdf_registration_form= pdfkit.from_string(registration_form_html,False,configuration=wkhtmltopdf_config,options=options)
    academic_progress_pdf = pdfkit.from_string(academic_progress_html, False, configuration=wkhtmltopdf_config, options=options)

    response = make_response(academic_progress_pdf)
    response.headers['Content-Type'] = "application/pdf"
    response.headers['Content-Disposition'] = "attachment; filename=output.pdf"

    return response


def _get_special_case_classes_from_json(classes_json):
    special_case_classes = list()

    for semester_classes in classes_json.values():

        for class_data in semester_classes:

            if "type" in class_data and class_data["type"] == "special_case":
                special_case_classes.append(class_data)

    return special_case_classes


def _get_career_data_from_json(career_json):
    return {
        'name': career_json["name"],
        'director': career_json["director"]
    }


@app.route('/careers', methods=['GET'])
@cross_origin()
def get_careers():
    with open('app.json', encoding="UTF8") as json_file:
        data = json.load(json_file)
        return data


if __name__ == "__main__":
    app.run(debug=True)
